// http://myjson.com/vb7mq
const QUIZ_QUESTIONS_URL = 'http://api.myjson.com/bins/vb7mq';

// get elements by their id
let quizButton = document.getElementById("button");
let quizDiv = document.getElementById("quizquestions");

// create a new instance of XMLHttpRequest object
// we'll use that insctance to establish a connection with URL that is specified
// and it'll let us to send or receive data
let quizRequest = new XMLHttpRequest();

// a click on the button starts the function and the quiz
quizButton.addEventListener("click", quizFunction);

// questions counter
let counter = 0;
let noOfCorrectAnswers = 0;

function quizFunction() {
    // initializes new request or re-initializes an existing one
    quizRequest.open('GET', QUIZ_QUESTIONS_URL);
    // sends request to server
    quizRequest.send();
    // transaction completes successfully
    quizRequest.onload = function() {
        console.log(quizRequest.responseText);
        // data received is string; when parsed becomes javascript object
        let quizQuestions = JSON.parse(quizRequest.responseText);
        // checking if can grab an element of array
        console.log(quizQuestions[0]);
        if (counter > 0) {
            try {
                answer = document.querySelector(`input[name="question${counter-1}"]:checked`).value;
                console.log('answer', answer);
                // extract string representation of number and convert to number
                answerIndex = Number(answer.slice(6));
                //console.log('slice', Number(answer.slice(6)));
            } catch (error) {
                alert('Please select one of the answers.');
                return;
            }
            if (quizQuestions[counter-1].answers[answerIndex] === quizQuestions[counter-1].correct) {
                noOfCorrectAnswers++;
                console.log('no of correct', noOfCorrectAnswers);
            }
        }
        // if counter over array index
        if (counter > quizQuestions.length - 1) {
            console.log(noOfCorrectAnswers);            
            alert(`Number of correct answers was: ${noOfCorrectAnswers}`);
            // reload page
            location.reload();
            return;
        }
        addToDiv(quizQuestions[counter]);
    }
    // send request for the content
}

function addToDiv(data) {
    let someString = "";
    someString += `<label>${data.question}</label><br>`;
    console.log('question', someString);
    //console.log(data.length);
    //console.log(data);
    for (let i = 0; i < data.answers.length; i++) {
        console.log(data.answers.length);
        //console.log(questions[i].answers[j]);
        someString += `<label><input class="radio" type="radio" name="question${counter}" value="${'answer' + i}">${data.answers[i]}</label><br>`;
        console.log(someString);
    }    
    quizDiv.innerHTML = someString;
    quizButton.value = `QUESTION No. ${counter + 1}`;
    counter++;
}
